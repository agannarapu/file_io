import java.io.Serializable;

public class User implements Serializable {

	String name;
	String profile;
	int age;
	String hobby;
	int salary;
	
	
	
	User(String name,String profile, int age,String hobby,int salary){
		this.name=name;
		this.profile=profile;
		this.age=age;
		this.hobby=hobby;
		this.salary=salary;
	}
	
	
	public  String toString() {
		return this.name+" "+this.profile+""+this.age+" "+this.hobby+" "+this.salary;
	}
}
