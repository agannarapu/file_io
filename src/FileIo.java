import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;

public class FileIo {
	public static void main(String[] args) throws Exception {
	   Scanner scn = new Scanner(System.in);
	   
	   File file =new File("user.txt");
	   ArrayList<User> lists =new ArrayList<User>();
	   ObjectOutputStream obj= null;
	   ObjectInputStream obj1=null;
	   ListIterator li=null;
	   
	   
	   if(file.isFile()) {
		   obj1=new ObjectInputStream(new FileInputStream(file));
		   lists =(ArrayList<User>)obj1.readObject();
		   obj1.close();
				 
			   
		   
	   }
	   int choice=-1;
	   
	   do {
		   System.out.println("1.Insert");
		   System.out.println("2.Display");
		   System.out.println("3.Exit");
		   
		   System.out.print("Enter your choice:");
		   choice = scn.nextInt();
		   
		   switch(choice) {
		   case 1:
			      System.out.println("How many users do you want:");
			      int num =scn.nextInt();
			      for(int i=0;i<num;i++) {
			      System.out.println("Enter name:");
		          String name=scn.next();
		          
		          System.out.println("Enter profile:");
		          String profile=scn.next();
		          
		          System.out.println("Enter age:");
		          int age=scn.nextInt();
		          
		          System.out.println("Enter hobby:");
		          String hobby=scn.next();
		          
		          System.out.println("Enter Salary:");
		          int  salary=scn.nextInt();
		          
		          lists.add(new User(name,profile,age,hobby,salary));
		          
						
			      }
			      try {
						obj=new ObjectOutputStream(new FileOutputStream(file));
						obj.writeObject(lists);
						obj.close();
						
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						
						e.printStackTrace();
					}
			    break;
		   case 2:
			       System.out.println("==================================");
			       li=lists.listIterator();
			       while(li.hasNext())
			       System.out.println(li.next());
			       System.out.println("================================");
			       break;
		   }
			      
	   }while(choice!=3);
	}
}
	   
		
	
